use std::borrow::{Borrow, Cow};

use crate::utils::parse_util;

#[derive(Debug)]
pub struct KeyValue<'a, T> {
    pub key: Cow<'a, str>,
    pub value: T,
}

impl<'a, T> KeyValue<'a, T> {
    pub fn new(key: Cow<'a, str>, value:T) -> KeyValue<'a, T> {
        KeyValue {key, value}
    }

    pub fn key(&self) -> &str {
        &self.key
    }

    pub fn value(&self) -> &T {
        self.value.borrow()
    }
}

impl<'a, T:Clone> Clone for KeyValue<'a, T> {
    fn clone(&self) -> Self {
        Self { key: self.key.clone(), value: self.value.clone() }
    }
}

impl<'a, T:ToString> KeyValue<'a, T> {
    pub fn to_string_with_delimiter(&self, delimiter:&str) -> String {
        parse_util::join2(&self.key, &self.value.to_string(), delimiter)
    }
}

impl<'a, T:ToString> ToString for KeyValue<'a, T> {
    fn to_string(&self) -> String {
        self.to_string_with_delimiter(":")
    }
}

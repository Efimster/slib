use std::net::TcpStream;
use std::fs::File;
use std::io::{Stdout, stdout, Stderr, stderr};

pub trait TryClone {
    fn try_clone(&self) -> Result<Self, Box<dyn std::error::Error>> where Self: Sized;
}

impl TryClone for TcpStream {
    fn try_clone(&self) -> Result<Self, Box<dyn std::error::Error>> {
        self.try_clone().map_err(|err|err.into())
    }
}

impl TryClone for File {
    fn try_clone(&self) -> Result<Self, Box<dyn std::error::Error>> {
        self.try_clone().map_err(|err|err.into())
    }
}

impl TryClone for Stdout {
    fn try_clone(&self) -> Result<Self, Box<dyn std::error::Error>> {
        Ok(stdout())
    }
}

impl TryClone for Stderr {
    fn try_clone(&self) -> Result<Self, Box<dyn std::error::Error>> {
        Ok(stderr())
    }
}

#[cfg(test)]
mod test {

    #[test]
    fn test_try_clone() {
    }
}
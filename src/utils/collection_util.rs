#[macro_export]
macro_rules! hashmap {
    ($( $key: expr => $val: expr ),*) => {{
         let mut map = ::std::collections::HashMap::new();
         $( map.insert($key, $val); )*
         map
    }}
}

//turns directly into a memset call, so expected to be fast
pub fn fill<'a, T:Copy + 'a>(iter:impl Iterator<Item = &'a mut T>, value:T){
    iter.for_each(|m| *m = value)
}

pub fn fill_default<'a, T:Default + 'a>(iter:impl Iterator<Item = &'a mut T>){
    iter.for_each(|m| *m = T::default())
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_fill(){
        let mut result = [0usize; 10];
        fill(result.iter_mut(), 5);
        assert_eq!(result[0], 5);
    }
}

//cargo +nightly bench --features bench  
#[cfg(test)]
#[cfg(feature = "bench")]
mod benches {
    extern crate test;
    use test::Bencher;
    use super::*;

    #[bench]
    fn bench_fill(bencher: &mut Bencher) {
        let mut result = [0u8; 100000];
        bencher.iter(|| {
            test::black_box(1);
            fill(result.iter_mut(), 0);
        });
    }

    #[bench]
    fn bench_fill_zeros(bencher: &mut Bencher) {
        let mut result = [0u8; 100000];
        bencher.iter(|| {
            test::black_box(1);
            fill_default(result.iter_mut());
        });
    }
}





use core::str;
use std::io::{BufRead, ErrorKind, Read};
use std::{cmp, io, vec};

use super::collection_util::fill;

pub const CRLF: &[u8] = &[0xd, 0xa];

pub fn split2<'a, 'b>(string:&'a str, delimiter:&'b str) -> Option<(&'a str, &'a str)> {
    let line:Box<[&str]> = string.splitn(2, delimiter).collect::<Box<[&str]>>();
    if line.len() < 2 {
        return None;
    }
    Some((line[0].trim(), line[1].trim()))
}

pub fn join2(left:&str, right:&str, delimiter:&str) -> String{
    format!("{}{}{}", left, delimiter, right)
}

pub fn read_until_pattern(reader:&mut impl BufRead, pattern:&[u8], to:&mut Vec<u8>) -> io::Result<usize> {
    let original_len = to.len();
    let mut remnants = vec![];
    let mut remnants_ref = &remnants as &[u8];
    let mut chain_reader = remnants_ref.chain(&mut *reader);

    let mut i = 0;
    while i < pattern.len(){
        let read_len = chain_reader.read_until(pattern[0], to)?;
        i = 1;
        let c_len = to.len();
        while i < pattern.len() {
            to.push(0);
            let len = to.len();
            let last = &mut to[len - 1..];
            match chain_reader.read_exact(last){
                Ok(_) => (),
                Err(ref err) if err.kind() == ErrorKind::UnexpectedEof => {
                    i = pattern.len();
                    to.truncate(len - 1);
                    break;
                },
                Err(err) => return Err(err),
            }
            if last[0] != pattern[i] {
                if remnants_ref.len() <= read_len {
                    remnants = to[c_len..].to_vec();
                    remnants_ref = &remnants;
                } else {
                    remnants_ref = &remnants_ref[read_len..]
                }
                to.truncate(c_len);
                chain_reader = remnants_ref.chain(&mut *reader);
                break;
            }
            i += 1;
        }
    }

    Ok(to.len() - original_len)
}

pub fn latin1_to_string(s: &[u8]) -> String {
    s.iter().map(|&c| c as char).collect()
}

pub fn read_until_any(reader:&mut impl BufRead, check_set:&[u8], to:&mut Vec<u8>) -> io::Result<(u8, usize)> {
    let mut total_size = 0;
    let mut buf = [0u8; 1];
    loop {
        match reader.read_exact(&mut buf) {
            Ok(_) => {
                total_size += 1;
                to.push(buf[0]);
                if check_set.contains(&buf[0]) {
                    return Ok((buf[0], total_size))
                }
            },
            Err(err) => return Err(err),
        }
    }
}

pub fn read_until_any_pattern<'c>(reader:&mut impl Read,
      check_set:&[&'c[u8]], to:&mut Vec<u8>) -> io::Result<(&'c [u8], usize)> {
    let mut total_size = 0;
    let mut buf = [0u8; 1];
    let mut matched_counts = vec![0usize; check_set.len()];
    let mut remnants = vec![];
    let mut remnants_ref = &remnants as &[u8];
    let mut chain_reader = remnants_ref.chain(&mut *reader);
    let mut part_to = vec![];

    loop {
        match chain_reader.read_exact(&mut buf) {
            Ok(_) => {
                let ch = buf[0];
                part_to.push(buf[0]);
                for i in 0 .. check_set.len() {
                    let word = check_set[i];
                    let index = matched_counts[i];
                    if index < word.len() && ch == word[index] {
                        if index == part_to.len() - 1 {
                            matched_counts[i] += 1;
                        }
                        
                        if matched_counts[i] == check_set[i].len() {
                            total_size += part_to.len();
                            to.extend_from_slice(&part_to);
                            return Ok((check_set[i], total_size))
                        }
                    }
                    else {
                        if i == check_set.len() - 1 && matched_counts.iter().all(|&c|c < part_to.len()){
                            total_size += 1;
                            fill(matched_counts.iter_mut(), 0);
                            to.push(part_to[0]);
                            remnants_ref = &remnants_ref[cmp::min(part_to.len(), remnants_ref.len()) .. ];
                            part_to.extend_from_slice(remnants_ref);
                            remnants = part_to[1..].to_vec();
                            remnants_ref = &remnants;
                            part_to = vec![];
                            chain_reader = remnants_ref.chain(&mut *reader);
                        }
                    }
                }
            },
            Err(err) => return Err(err),
        }
    }
}

pub fn read_while_any(reader:&mut impl BufRead, check_set:&[u8], to:&mut Vec<u8>) -> io::Result<(u8, usize)> {
    let mut total_size = 0;
    let mut buf = [0u8; 1];
    loop {
        match reader.read_exact(&mut buf) {
            Ok(_) => {
                if !check_set.contains(&buf[0]) {
                    return Ok((buf[0], total_size))
                }
                to.push(buf[0]);
                total_size += 1;
            },
            Err(err) => return Err(err),
        }
    }
}

pub fn find_any<'a, 'b,  T:PartialEq>(input:&'a [T], check_set:&'b [T]) -> Option<(&'a T, usize)> {
    for i in  0 .. input.len(){
        if check_set.contains(&input[i]) {
            return Some((&input[i], i))
        }
    }
    None
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::str;
    use std::io;
    pub const CDATA_TAG:&str = "![CDATA[";
    pub const QUESTION_MARK:u8 = "?".as_bytes()[0];
    pub const COMMENT_TAG:&str = "!--";

    #[test]
    fn test_read_until() -> io::Result<()> {
        let mut string = "<a b:b1 c:c1 d:d1 />\n<a b:b2 c:c2 d:d2 />".as_bytes();
        let mut to = Vec::new();
        let size = read_until_pattern(&mut string, "c:c2".as_bytes(), &mut to)?;
        str::from_utf8(&to).unwrap();
        assert_eq!(str::from_utf8(&to).unwrap() ,"<a b:b1 c:c1 d:d1 />\n<a b:b2 c:c2");
        assert_eq!(size ,"<a b:b1 c:c1 d:d1 />\n<a b:b2 c:c2".len());

        let mut string = "<a b:b1 c:c1 d:d1 />\n<anode b:b2 c:c2 d:d2 />".as_bytes();
        let mut to = Vec::new();
        let size = read_until_pattern(&mut string, "<anode".as_bytes(), &mut to)?;
        assert_eq!(str::from_utf8(&to).unwrap() ,"<a b:b1 c:c1 d:d1 />\n<anode");
        assert_eq!(size ,"<a b:b1 c:c1 d:d1 />\n<anode".len());

        let mut string = "<a b:b1 c:c1 d:d1 />\n<anode b:b2 c:c2 d:d2 />".as_bytes();
        let mut to = Vec::new();
        let size = read_until_pattern(&mut string, "<bnode".as_bytes(), &mut to)?;
        assert_eq!(string , "".as_bytes());
        assert_eq!(to , "<a b:b1 c:c1 d:d1 />\n<anode b:b2 c:c2 d:d2 />".as_bytes());
        assert_eq!(size, 45);

        let mut string = "1-x-xx2".as_bytes();
        let mut to = Vec::new();
        let size = read_until_pattern(&mut string, "-xx".as_bytes(), &mut to)?;
        assert_eq!(string , "2".as_bytes());
        assert_eq!(to , "1-x-xx".as_bytes());
        assert_eq!(size, 6);

        let mut string = "$1131132$".as_bytes();
        let mut to = Vec::new();
        let size = read_until_pattern(&mut string, "1132".as_bytes(), &mut to)?;
        assert_eq!(string , "$".as_bytes());
        assert_eq!(to , "$1131132".as_bytes());
        assert_eq!(size, 8);

        let mut string = "12".as_bytes();
        let mut to = Vec::new();
        let size = read_until_pattern(&mut string, "13".as_bytes(), &mut to)?;
        assert_eq!(string , "".as_bytes());
        assert_eq!(to , "12".as_bytes());
        assert_eq!(size, 2);

        let mut string = "222222".as_bytes();
        let mut to = Vec::new();
        let size = read_until_pattern(&mut string, "33333333".as_bytes(), &mut to)?;
        assert_eq!(string , "".as_bytes());
        assert_eq!(to , "222222".as_bytes());
        assert_eq!(size, 6);


        Ok(())
    }

    #[test]
    fn test_read_while_any() -> io::Result<()> {
        let mut string = "aaaaabbbccc".as_bytes();
        let mut to = Vec::new();
        let up_to = ["a".as_bytes()[0], "b".as_bytes()[0]];
        let (byte, _) = read_while_any(&mut string, &up_to, &mut to)?;
        assert_eq!(str::from_utf8(&to).unwrap() ,"aaaaabbb");
        assert_eq!(byte ,"c".as_bytes()[0]);
        Ok(())
    }

    #[test]
    fn test_read_until_any() -> io::Result<()> {
        let mut string = "123456789".as_bytes();
        let mut to = Vec::new();
        let check_set = ["4".as_bytes()[0], "3".as_bytes()[0]];
        let (sep, _) = read_until_any(&mut string, &check_set, &mut to)?;
        assert_eq!(str::from_utf8(&to).unwrap() , "123");
        assert_eq!(str::from_utf8(&[sep]).unwrap() , "3");
        Ok(())
    }

    #[test]
    fn test_read_until_any_pattern() -> io::Result<()> {
        let mut string = "![CD!-![CDATA!--?abcd".as_bytes();
        let mut to = Vec::new();
        let up_to = [CDATA_TAG.as_bytes(), COMMENT_TAG.as_bytes(), &[QUESTION_MARK]];
        let (patter, size) = read_until_any_pattern(&mut string, &up_to, &mut to)?;
        assert_eq!(str::from_utf8(&patter).unwrap() , COMMENT_TAG);
        assert_eq!(size, "![CD!-![CDATA!--".len());
        assert_eq!(str::from_utf8(&to).unwrap() , "![CD!-![CDATA!--");
        Ok(())
    }

    #[test]
    fn test_read_until_any_pattern_3() -> io::Result<()> {
        let string = r##"<div class="1" >
            <div class="2">
                <div class="3">
                    <div class="4"></div>
                </div>
            </div>
            <div class="5"></div>
        </div>
        "##;
        
        let tag = "div";
        let mut to = vec![];
        let open_pattern = format!("<{}", tag).into_bytes();
        let close_pattern = format!("</{}>", tag).into_bytes();
        let check_set:&[&[u8]] = &[&close_pattern, &open_pattern];
        let reader = &mut string.as_bytes();
        reader.read_until(">".as_bytes()[0], &mut to)?;
        let (pattern, _size) = read_until_any_pattern(reader, check_set, &mut to)?;
        assert_eq!(str::from_utf8(&pattern).unwrap(), "<div");
        Ok(())
    }

    #[test]
    fn test_read_until_any_pattern_2() -> io::Result<()> {

        let mut string = "$1131132$".as_bytes();
        let mut to = Vec::new();
        let (_pattern, size) = read_until_any_pattern(&mut string, &["1132".as_bytes(), "115".as_bytes()], &mut to)?;
        assert_eq!(string , "$".as_bytes());
        assert_eq!(to , "$1131132".as_bytes());
        assert_eq!(size, 8);

        let string = "<a />\
            <b></b>\
        </link>";
        let mut to = Vec::new();
        let (pattern, size) = read_until_any_pattern(&mut string.as_bytes(), &["</link>".as_bytes(), "<link>".as_bytes()], &mut to)?;
        assert_eq!(to , string.as_bytes());
        assert_eq!(std::str::from_utf8(pattern).unwrap(), "</link>");
        assert_eq!(size, string.len());

        let mut string = "1</link>".as_bytes();
        let mut to = Vec::new();
        let result = read_until_any_pattern(&mut string, &["123".as_bytes(), "890".as_bytes()], &mut to);
        assert_eq!(to , "1</link>".as_bytes());
        assert!(matches!(result, Err(_)));
        assert_eq!(result.unwrap_err().kind(), io::ErrorKind::UnexpectedEof);

        Ok(())
    }
}
